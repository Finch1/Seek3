#include<map>
#include<vector>
#include<list>
#include<iostream>
#include<string>
namespace Seek{
    class BattleScene;

    class BattleObject{
        public:
            struct SkillContent{
                std::string Name;
                std::string Summary;
                SkillContent(std::string name,std::string summary):Name(name),Summary(summary){}
            }; 
            struct Damage{
                unsigned int Type;
                float Hurt;
                BattleObject * From;
                Damage(unsigned int type,float hurt,BattleObject * from):Type(type),Hurt(hurt),From(from){}
            };
        public:
            BattleObject():enable(true){}
            ~BattleObject(){}
            virtual void InitScene();
            virtual void BeforeAttack(int SkillIndex);
            virtual void OnAttack(int SkillIndex);
            virtual void AfterAttack();
            virtual void BeforeDefend(BattleObject *from);
            virtual void OnDefend(BattleObject *from);
            virtual void AfterDefend(BattleObject *from);
            virtual void DestoryScene(){}
            virtual void Skill(int SkillIndex){}
        public:
            std::list<BattleObject *> m_child;
            std::vector<SkillContent> m_skill_content;
            std::map<std::string,float> m_value;
            BattleScene *m_scene;
            std::list<BattleObject *> m_target;
            std::list<Damage> DamagePool;
            bool enable;
        private:
        protected:
    };

    class BattleScene{
    public:
        struct BattleSkill{
            BattleObject::SkillContent SkillContent;
            BattleObject *Obj;
            int SkillIndex;
            BattleSkill(BattleObject::SkillContent skillContent, BattleObject *obj,int skillIndex):SkillContent(skillContent),Obj(obj),SkillIndex(skillIndex){}
        };

        void Start();
        void AddChild(BattleObject *Obj);
        void Update();
        void UseSkill(BattleObject *Obj,int SkillIndex);
        void SetCurrent(int ObjectIndex);
        void TestOutSkill();
        std::vector<BattleSkill> GetSkill();
        void AddTarget(BattleObject *Obj);

    private:
        std::vector<BattleObject *> m_child;
        unsigned int Current;
    private:
        void GetSkill_Sub(std::vector<BattleSkill> & skills,BattleObject *obj);
    };

    class Pet : public BattleObject{
        public:
            void InitScene();
            void Skill(int SkillIndex);
            void BeforeAttack(int SkillIndex);
            void OnAttack(int SkillIndex);
    };

}