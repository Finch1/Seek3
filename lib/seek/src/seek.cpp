#include<seek.hpp>
namespace Seek{
    void BattleScene::AddChild(BattleObject *Obj){
        m_child.push_back(Obj);
        Obj->m_scene = this;
        Obj->InitScene();
    }
    void BattleScene::UseSkill(BattleObject *Obj,int SkillIndex){
        Obj->BeforeAttack(SkillIndex);
        //Obj->Skill(SkillIndex);
        //Obj->AfterAttack();
    }
    void BattleScene::TestOutSkill(){//Test
        for(BattleObject::SkillContent i : m_child[Current]->m_skill_content){
            std::cout<<i.Name<<std::endl;
            std::cout<<i.Summary<<std::endl;
        }
    }
    void BattleScene::GetSkill_Sub(std::vector<BattleSkill> & skills,BattleObject *obj){
        int cnt = 0;
        for(BattleObject::SkillContent i : obj->m_skill_content){
            skills.push_back(BattleSkill(i,obj,cnt));
            cnt++;
        }
        for(BattleObject * i : obj->m_child){
            GetSkill_Sub(skills,i);
        }
    }
    std::vector<BattleScene::BattleSkill> BattleScene::GetSkill(){
        std::vector<BattleSkill> ret;
        GetSkill_Sub(ret,m_child[Current]);
        return ret;
    }
    void Pet::InitScene(){
        m_skill_content.push_back(BattleObject::SkillContent("skill0","this is the first summary"));
        m_skill_content.push_back(BattleObject::SkillContent("skill1","this is the second summary"));
        m_value["blood"] = 1000;
        m_value["attack"] = 600;
        m_value["defense"] = 300;
    }
    void Pet::Skill(int SkillIndex){
        switch(SkillIndex){
            case 0:
                std::cout<<"you call skill 0"<<std::endl;
                break;
            case 1:
                std::cout<<"you call skill 1"<<std::endl;
                break;
        }

    }
    void Pet::OnAttack(int SkillIndex){
        BattleObject::OnAttack(SkillIndex);
        Skill(SkillIndex);
        for(BattleObject * i : m_target){
            i->OnDefend(this);
        }
    }
    void Pet::BeforeAttack(int SkillIndex){
        BattleObject::BeforeAttack(SkillIndex);
        //play animation
        for(BattleObject * i : m_target){
            i->BeforeDefend(this);
        }
        OnAttack(SkillIndex);
    }
    void BattleScene::SetCurrent(int ObjectIndex){
        Current = ObjectIndex;
    }
    void BattleObject::InitScene(){
        for(BattleObject *i : m_child){
            if(i->enable)i->InitScene();
        }
    }
    void BattleObject::BeforeAttack(int SkillIndex){
        for(BattleObject *i : m_child){
            if(i->enable)i->BeforeAttack(SkillIndex);
        }
    }
    void BattleObject::OnAttack(int SkillIndex){
        for(BattleObject *i : m_child){
            if(i->enable)i->OnAttack(SkillIndex);
        }
    }
    void BattleObject::AfterAttack(){
        for(BattleObject *i : m_child){
            if(i->enable)i->AfterAttack();
        }
    }
    void BattleObject::BeforeDefend(BattleObject *from){
        for(BattleObject *i : m_child){
            if(i->enable)i->BeforeDefend(from);
        }
    }
    void BattleObject::OnDefend(BattleObject *from){
        for(BattleObject *i : m_child){
            if(i->enable)i->OnDefend(from);
        }
    }
    void BattleObject::AfterDefend(BattleObject *from){
        for(BattleObject *i : m_child){
            if(i->enable)i->AfterDefend(from);
        }
    }
}