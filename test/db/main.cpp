/** @file DragonBoy.cpp
 ** @author Piotr Krupa (piotrkrupa06@gmail.com)
 ** @license MIT License
 **/

#include <SFML/Graphics.hpp>

#include <dragonBones/SFMLFactory.h>
#include <dragonBones/SFMLArmatureDisplay.h>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1024, 768), "Seek");
	window.setFramerateLimit(60);

	dragonBones::SFMLFactory factory;

	sf::Texture texture;
	texture.loadFromFile("resources/Demon_tex.png");

	factory.loadDragonBonesData("resources/Demon_ske.json");
	factory.loadTextureAtlasData("resources/Demon_tex.json", &texture);

	auto armatureDisplay = new dragonBones::SFMLArmatureDisplay("armatureName");
	armatureDisplay->getAnimation()->play("steady");
	armatureDisplay->setPosition({ 600.f, 600.f });

	sf::Clock clock;

	while (window.isOpen())
	{
		float deltaTime = clock.restart().asSeconds();

		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		factory.update(deltaTime);

		window.clear();
		window.draw(*armatureDisplay);
		window.display();
	}
	
	return 0;
}
