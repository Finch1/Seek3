cmake_minimum_required(VERSION 3.1)
project(imgui_test)
add_executable(${PROJECT_NAME}
    main.cpp
)
target_link_libraries(${PROJECT_NAME}
    ${OPENGL_LIBRARY}
    sfml-audio
    sfml-graphics
    sfml-main
    sfml-network
    sfml-system
    sfml-window
    imgui-sfml
)