#include<seek.hpp>
int main(){
	Seek::Pet p;
	Seek::BattleScene s;
	s.AddChild(&p);
	s.SetCurrent(0);
	//s.TestOutSkill();
    std::vector<Seek::BattleScene::BattleSkill> ret = s.GetSkill();
    for(Seek::BattleScene::BattleSkill i : ret){
        std::cout<<i.SkillContent.Name<<std::endl;
        s.UseSkill(i.Obj,i.SkillIndex);
    }
	//s.UseSkill(0); 
	return 0;
} 